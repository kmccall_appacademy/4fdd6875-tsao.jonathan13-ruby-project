# Hey! We'll start out with a few very simple exercises.
# Please don't skip these, even if they seem trivial;
# they will also teach you the expected workflow, and how
# to test your code with rspec.

def hello #solved
  "Hello!"
end

def greet(someone) #solved
  "Hello, #{someone}!"
end
